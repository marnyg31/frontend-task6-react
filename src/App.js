import React from "react";
import "./App.css";
import { Switch, BrowserRouter, Route } from "react-router-dom";
import Navbar from "./Components/Navbar";
import "../node_modules/bulma/css/bulma.min.css";
import Login from "./Pages/Login";
import Profile from "./Pages/Profile";
import Translate from "./Pages/Translate";

class App extends React.Component {
  render() {
    return <BrowserRouter>
      <div className="App">
        <Navbar></Navbar>
        <div className="container ">
          <Switch >
            <Route exact path="/" component={Translate} />
            <Route path="/profile" component={Profile} />
            <Route path="/login" component={Login} />
          </Switch>
        </div>
      </div>
    </BrowserRouter >;
  }
}

export default App;
