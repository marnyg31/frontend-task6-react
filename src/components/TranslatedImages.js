import React from 'react';
class TranslatedImages extends React.Component {
  state = {};

  render() {
    return (
      <div style={this.props.preview? {opacity: 0.5}:{}} >
        {this.displayTextAsImages(this.props.text)}

      </div>
    );
  }

  displayTextAsImages(text) {
    if (this.state.images) {
      return text.split('').map((char, index) => {
        let img = this.state.images[char + ".png"];
        if (img)
          return <img alt="translatedImage" key={index} width="50" src={img} ></ img >
        else
          return <img alt="translatedImage" key={index} width="50" src="https://images.vexels.com/media/users/3/152864/isolated/preview/2e095de08301a57890aad6898ad8ba4c-yellow-circle-question-mark-icon-by-vexels.png"></ img>;
      });
    }
    else { return <p>Error</p>; }
  }

  componentDidMount() {
    function importAll(r) {
      let images = {};
      r.keys().forEach((item, index) => { images[item.replace('./', '')] = r(item); });
      return images;
    }
    const imgs = importAll(require.context('../Resources/individial_signs', false, /\.(png|jpe?g|svg)$/));
    this.setState({ images: imgs });
  }

}

export default TranslatedImages;