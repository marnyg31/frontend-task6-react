import React from 'react';
import { NavLink } from "react-router-dom";
import Logo from '../Resources/Logo.png';
import { logout } from '../Services/Auth';

class Navbar extends React.Component {
  state = {};

  render() {
    return (
      <nav className="navbar is-light " role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <NavLink className="navbar-item" to="/">
            <img src={Logo} alt="Logo" />
          </NavLink>
        </div>

        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <NavLink className="navbar-item" to="/">Translate</NavLink>
            <NavLink className="navbar-item" to="/profile">Profile</NavLink>
          </div >

          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <button className="button" onClick={logout}>Log out</button>
              </div>
            </div>
          </div >
        </div >
      </nav >
    );
  }
}

export default Navbar;