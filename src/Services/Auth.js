
export function authIsOk() {
  return Boolean(localStorage.getItem("name"));
}

export function logout() {
  localStorage.removeItem("name");
  window.history.go("/login");
}

export function login(name) {
  localStorage.setItem("name", name);
}

export function getUser() {
  return localStorage.getItem("name");
}