import { getUser } from "./Auth";

export function addToHistory(text) {
  let user = getUser();
  let hist = JSON.parse(localStorage.getItem(user + "_history"));

  if (!hist) { hist = []; }
  if (hist.length === 10) { hist.shift(); }

  hist.push(text);
  localStorage.setItem(user + "_history", JSON.stringify(hist));
}
export function clearHistory() {
  let user = getUser();
  localStorage.removeItem(user + '_history');
}

export function getHistory() {
  let user = getUser();
  let hist=JSON.parse(localStorage.getItem(user + "_history"));
  if(!hist) hist=[]
  return hist
}