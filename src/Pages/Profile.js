import React from "react";
import { authIsOk, logout } from '../Services/Auth';
import { Redirect } from 'react-router-dom';
import { getHistory, clearHistory } from '../Services/History';
import TranslatedImages from "../Components/TranslatedImages";

class Profile extends React.Component {
    state = { history: getHistory() };
    
    render() {
        if (!authIsOk())
            return <Redirect to='/login' />;
        return (
            <div className="">
                <br />
                <span className="title">Profile  </span >
                <button className="button" onClick={() => this.onClearHistory()}>Clear Hisory</button>
                <span> </span>
                <button className="button" onClick={logout} >logout</button>
                <br /><br /><br />
                <ul className="content">{this.getHistoryList()}</ul>
            </div>
        );
    }
    onClearHistory() {
        clearHistory();
        this.setState({ history: [] });
    }
    getHistoryList() {
        return this.state.history.reverse().map((el, i) => {
            return <>
                <div className=" has-background-primary-light panel columns">
                    <div className="column">
                        <span>Nr: {i}</span>
                        <li className="text" key={i}>{el}</li>
                    </div>
                    <div className="column">
                        <TranslatedImages text={el}></TranslatedImages>
                    </div>
                </div>
                <br /></>;
        });

    }
}

export default Profile;
