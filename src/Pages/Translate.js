import React from "react";
import { authIsOk } from '../Services/Auth';
import { Redirect } from 'react-router-dom';
import { addToHistory } from '../Services/History';
import TranslatedImages from "../Components/TranslatedImages";
// import * as imgs from '../Resources/individial_signs'


class Translate extends React.Component {
    state = { inputValue: "", preview: true };

    render() {
        if (!authIsOk())
            return <Redirect to='/login' />;
        return (
            <>
                <br />
                <h1 className="title ">What do you want to translate?</h1>
                <br />
                <div className="container has-background-primary-light panel columns">
                    <div className="column">
                        <h3 className="title">Input</h3>
                        <div className="field has-addons">
                            <div className="control">
                                <input className="input" placeholder="Enter text" value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)} type="text" name="name" />
                            </div>

                            <div className="control">
                                <button className="button is-info" onClick={() => this.onDoneClick()}>Done</button>
                            </div>
                        </div>
                    </div>
                    <div className="panel column">
                        <h3 className="title">Translated</h3>
                        <div id="translatedImages">{this.state.preview ? "Preview" : ""}</div>
                        <TranslatedImages
                            preview={this.state.preview}
                            text={this.state.inputValue}>
                        </TranslatedImages>
                    </div>
                </div>
            </>
        );
    }

    onDoneClick() {
        addToHistory(this.state.inputValue);
        this.setState({ preview: false });
    }

    updateInputValue(evt) {
        this.setState({
            inputValue: evt.target.value,
            preview: true
        });
    }
}

export default Translate;
