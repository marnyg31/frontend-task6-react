import React from "react";
import { login } from '../Services/Auth';

class Login extends React.Component {
  state = { inputValue: "" };

  render() {
    return (
      <div className="container">
          <br/>
          <br/>
        <div className="columns is-centered">
          <div className="column has-background-primary-light panel is-one-quarter is-centered">
            <h1 className="title has-text-centered">Login</h1><br/>
            <div className="field has-addons  ">
              <div className="control is-expanded">
                <input className="input" placeholder="Enter username" value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)} type="text" name="name" />
              </div>
              <div className="control">
                <button className="button is-info" onClick={() => this.onLoginClick()}>Done</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  updateInputValue(evt) {
    this.setState({
      inputValue: evt.target.value
    });
  }
  onLoginClick() {
    if (this.state.inputValue) {
      login(this.state.inputValue);
      this.props.history.push('/');
    } else {
      alert("enter username");
    }
  }
}

export default Login;
